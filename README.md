# bf3mf-legacy

This is a battlefield 3 matchmaking platform for clans. 
This project was written in 2012, the code quality is super low since I was just getting started with PHP and programming in general. Nevertheless, it was able to fulfill its purpose.
All of this was written with a custom MVC-like system before I even knew what MVC was. 

It supports:

* Detailed matchmaking with a number of filters
* Match/Clan-war arrangement system
* Team profiles with custom avatars, descriptions, etc
* Private messaging between team admins
* Notifications system
* And more...
